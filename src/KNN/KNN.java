/*
 * Programmed by Muhammad Ahsan
 * muhammad.ahsan@gmail.com
 * Research Intern
 * Yahoo! Research Barcelona
 * Spain
 */
package KNN;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.neighboursearch.LinearNNSearch;

public class KNN {

    public static void main(String[] args) {
        boolean debug = true;
        if (args.length < 3) {
            System.out.println("Enter dataset path and Instace path to classified and K");
            System.exit(0);
        }

        Instances dataset = null;
        Instances activeUsers;
        Instance activeUser = null;

        //loop through the file and read instances
        try {
            BufferedReader bufRead = new BufferedReader(
                    new FileReader(args[0]));
            dataset = new Instances(bufRead);
            bufRead.close();

        } catch (IOException ioe) {
            System.out.println("Error reading file '" + args[0] + "' ");
            if (debug) {
                // ioe.printStackTrace();
            } else {
                System.out.println(ioe.getMessage());
            }
            System.exit(0);
        }

        try {
            BufferedReader bufRead = new BufferedReader(
                    new FileReader(args[1]));
            activeUsers = new Instances(bufRead);
            @SuppressWarnings("unchecked")
            Enumeration<Instances> en = activeUsers.enumerateInstances();
            activeUser = (Instance) en.nextElement();
            bufRead.close();

        } catch (IOException ioe) {
            System.out.println("Error reading file '" + args[1] + "' ");
            if (debug) {
                // ioe.printStackTrace();
            } else {
                System.out.println(ioe.getMessage());
            }
            System.exit(0);
        }
        // Searcher
        // Class implementing the brute force search algorithm for nearest neighbour search
        LinearNNSearch kNN = new LinearNNSearch(dataset);
        Instances neighbors;
        double[] distances;

        try {
            neighbors = kNN.kNearestNeighbours(activeUser, 5);
            distances = kNN.getDistances();
        } catch (Exception e) {
            System.out.println("Neighbors could not be found.");
            return;
        }

        System.out.println("Similarity and Distances are based on"
                + " LinearNNSearch.getDistances() of weka. You can also modify");
        double[] similarities = new double[distances.length];
        for (int i = 0; i < distances.length; i++) {
            similarities[i] = 1.0 / distances[i];
            if (debug) {
                System.out.println("Distance = " + distances[i] + " and Similarity = " + similarities[i]);
            }
        }

        @SuppressWarnings("unchecked")
        Enumeration<Instances> nInstances = neighbors.enumerateInstances();

        System.out.println("Similar K users profiles are as under");
        // Looping through all nearest neighbours
        while (nInstances.hasMoreElements()) {
            Instance currNeighbor = (Instance) nInstances.nextElement();
            // Display users profile
            System.out.println(currNeighbor.toString());
        }
    }
}
